﻿<?php
#1 создать массив и заполнить его случайными числами от 1 до 100 (ф-я rand).
for ($i = 0; $i <= 10; $i++) {
    $arr[] = rand(1, 100);
}

#1.1  Далее, вычислить произведение тех элементов, которые больше нуля и у которых четные индексы
/**
 * @param array $arr
 * @param int $multiply
 * @return string
 */
function multiplyArr(array $arr, int $multiply) :string
{
    for ($i = 0; $i <= 10; $i++){
        if ($i > 0 && $i % 2 == 0) {
            $multiply *= $arr[$i];
        }
    }
    return $multiply;
}
echo multiplyArr($arr, 1);
echo "<br>";

#1.2 вывести на экран элементы, которые больше нуля и у которых нечетный индекс.
/**
 * @param array $arr
 * @return string
 */
function oddArr(array $arr) :string
{
    $odd = "";
    for ($i = 0; $i <= 10; $i++) {
        if ($i > 0 && $i % 2 != 0) {
            $odd .= $arr[$i] . ":";
        }
    }

    return $odd;
}
echo oddArr($arr);
echo "<hr>";
#2 Даны два числа. Найти их сумму и произведение. Даны два числа. Найдите сумму их квадратов.
/**
 * @param int $a
 * @param int $b
 * @param string $c
 * @return int
 */
function sumAndMultipy(int $a, int $b, string $c):int
{
    if ($c == '*') {
        $result = $a * $b;
    } elseif ($c == '+') {
        $result = $a + $b;
    } else {
        $result = $a * $a + $b * $b;
    }
    return $result;
}

echo sumAndMultipy(4, 5, '+');
echo "<br>";
echo sumAndMultipy(4, 5, '*');
echo "<br>";
echo sumAndMultipy(4, 5, ' ');
echo "<hr>";
#3 Даны три числа. Найдите их среднее арифметическое.
/**
 * @param int $a
 * @param int $b
 * @param int $c
 * @return int
 */
function searchMiddle(int $a, int $b, int $c):int
{
    return ($a + $b + $c) / 3;
}
echo searchMiddle(3, 6, 9);

#4 Дано число. Увеличьте его на 30%, на 120%.
function incrPercent(int $a, int $b):int
{
    return $a + (($a / 100) * $b);
}
echo incrPercent(4, 30);
echo "<br>";
echo incrPercent(4, 120);
echo "<hr>";

# 8 Заполнить массив длины n нулями и единицами, при этом данные значения чередуются, начиная с нуля.
/**
 * @param int $n
 * @return array
 */
function arrLong(int $n):array
{
    $arr = [];
    for ($i = 0; $i < $n; $i++) {
        ($i % 2 == 0) ? $arr[] = 0 : $arr[] = 1;
    }
    echo '<pre>';
    return($arr);
    echo '</pre>';
}
print_r(arrLong(8));
echo "<hr>";
# 9 Определите, есть ли в массиве повторяющиеся элементы.
/**
 * @param array $array
 * @return bool
 */
function showDubble(array $array):bool
{
    $new_array[] = 0;
    foreach ($array as $value) {
        if ($new_array[$value]){
            $new_array[$value] += 1;
        } else {
            $new_array[$value] = 1;
        }
    }
    foreach ($new_array as $key => $n) {
        echo $key;
        if ($n > 1)
            echo " - $n";
        echo "<br />";
    }
    return false;
}

showDubble($arr);
echo "<hr>";

# 10 Найти минимальное и максимальное среди 3 чисел
/**
 * @param int $a
 * @param int $b
 * @param int $c
 * @return bool
 */
function searchMaxMin(int $a, int $b, int $c):bool
{
    if ($a > $b && $a > $c) {
        echo "a максимальное = $a<br>";
    } elseif ($b > $a && $b > $c) {
        echo "b максимальное = $b<br>";
    } else {
        echo "c максимальное = $c<br>";
    }
    if ($a < $b && $a < $c) {
        echo "a минимальное = $a<br>";
    } elseif ($b < $a && $b < $c) {
        echo "b минимальное = $b<br>";
    } else {
        echo "c минимальное = $c<br>";
    }
    return false;
}
searchMaxMin(3, 6, 8);
echo "<hr>";

# 11 Найти площадь
/**
 * @param int $a
 * @param int $b
 * @return int
 */
function searchArea(int $a, int $b) :int
{
    return $a * $b;
}
echo searchArea(3, 5);
echo "<hr>";

# 12 Теорема Пифагора
/**
 * @param int $katet1
 * @param int $katet2
 * @return int
 */
function countPifagora(int $katet1, int $katet2):int
{
    $square_hypotenuse = ($katet1 * $katet1 + $katet2 * $katet2);
    return $hypotenuse = sqrt($square_hypotenuse);
}
echo countPifagora(4, 4);
echo "<hr>";

# 13 Найти периметр
/**
 * @param int $a
 * @param int $b
 * @return int
 */
function searchPerimeter(int $a, int $b):int
{
    return ($a + $b) * 2;
}
echo searchPerimeter(40, 60);
echo "<hr>";

# 14 Найти дискриминант
/**
 * @param int $b
 * @param int $a
 * @param int $c
 * @return int
 */
function searchDiscriminant(int $b, int $a, int $c):int
{
    return ($b ** 2) - (4 * $a * $c);
}

echo searchDiscriminant(4, 2, 3);
echo "<hr>";

# 15 Создать только четные числа до 100
for ($i = 0; $i <= 100; $i++) {
    if ($i % 2 == 0) {
        echo $num = $i . " : ";
    }
}
echo "<hr>";
# 16 Создать не четные числа до 100
for ($i = 0; $i <= 100; $i++) {
    if ($i % 2 != 0) {
        echo $num = $i . " : ";
    }
}
echo "<hr>";
/**
 * @param int $a
 * @param int $b
 * @return int
 */
# 17 Создать функцию по нахождению числа в степени
function searchPow(int $a, int $b):int
{
    return $a ** $b;
}
echo searchPow(2, 3);
echo "<hr>";

# 18 написать функцию сортировки. 
for ($i = 0; $i < 10; $i++) {
    $arr_rand[$i] = rand(1, 10);
}
/**
 * @param array $arr
 * @param string|null $par
 * @return array
 */
function sort_vs_rsort(array $arr, string $par = NULL):array
{
    if ($par !== "rsort") {
        sort($arr);
    } else {
        $par($arr);
    }
   
    return $arr;
  
}

echo "<br>";
print_r(sort_vs_rsort($arr_rand));
print_r(sort_vs_rsort($arr_rand, "rsort"));
echo "<hr>";



# 19 написать функцию поиска в массиве. функция будет принимать два параметра. Первый массив, второй поисковое число
/**
 * @param array $arr
 * @param int $search
 * @return string
 */
function searchArrays(array $arr, int $search):string
{
    foreach ($arr as $value) {
        if ($search == $value) {
            $arr_res[] = $value;
            $count = 0;
            foreach ($arr_res as $item) {
                $count++;
            }
        }
    }
    return "данное число встречается: " . $count;
}
echo searchArrays($arr_rand, 7);
